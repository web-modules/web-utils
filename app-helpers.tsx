import React from "react";
import { Link } from "react-router-dom";

export const DispArgs = ({ args }:any) => {
  // let { id } = useParams();
  // console.log(id);
  return (
    <>
      {/* <h2>Current url:</h2> */}
      <h2>Passed arguments:</h2>
      <ul>
        {Object.keys(args).map((elt, i) => (
          <li key={i}>
            {elt}: {args[elt] ? args[elt] : "unset!"}
          </li>
        ))}
      </ul>
    </>
  );
};

export const DispLinks = ({ links, basename = "", hardLink }:any) => {
  return (
    <ul>
      {Object.keys(links).map((elt, i) => (
        <li key={i}>
          {hardLink ? (
            <a href={basename + links[elt]}>{elt}</a>
          ) : (
            <Link to={basename + links[elt]}>{elt}</Link>
          )}
        </li>
      ))}
    </ul>
  );
};

// export const DispModulesRoutes = ({ modules, hardLink }:any) => {
//   return (
//     <ul>
//       {Object.keys(modules).map((elt, i) => (
//         <li key={elt}>
//           {hardLink ? (
//             <a href={basename + links[elt]}>{elt}</a>
//           ) : (
//             <Link to={basename + links[elt]}>{elt}</Link>
//           )}
//         </li>
//       ))}
//     </ul>
//   );
// };

/**
 * routing, url arguments and display usages
 */

export const UsagesDisplay = ({ argList, usagesRef }: any) => {
  return (
    <>
      <h2>Supported url arguments:</h2>
      <ul>
        {Object.keys(argList).map((element, i) => (
          <li key={i}>{element}</li>
        ))}
      </ul>
      <h2>Usage examples:</h2>
      <DispLinks links={usagesRef.current} hardLink />
      {/* <h2>Current passed arguments:</h2>
      <ul>
        {argList
          ? Object.keys(argList).map((elt) => (
              <li>
                {elt}: {argList[elt] ? argList[elt] : "unset!"}
              </li>
            ))
          : "none"}
      </ul> */}
    </>
  );
};
