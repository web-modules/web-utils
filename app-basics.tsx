/**The dependancies required by this module: */
// import { Children } from "react";
import React from "react";
import {
  BrowserRouter as Router,
  useLocation,
  Route,
  Routes,
  // Switch
} from "react-router-dom";

export const useQuery = () => {
  return new URLSearchParams(useLocation().search);
};

export const FillArgs = ({ argList, children }:any) => {
  let query = useQuery();

  // check for provided url params to prefill app args
  Object.keys(argList).forEach((argName: string) => {
    // console.log(query.getAll(".*"));
    const urlArgVal = query.get(argName);
    if (urlArgVal && urlArgVal !== "") argList[argName] = urlArgVal;
  });

  const args = { ...argList };

  return (
    <>
      {children}
      {/* <ArgsPreProcess supportedArgs={ARGS} />; */}
      {/* <Usages args={args} /> */}
    </>
  );
};

export const MakeRoutes = ({ modules, basename = "/" }:any) => {
  // let Module = SingleApp;
  // console.log(modules);
  return (
    <>
      <Routes>
        {Object.keys(modules).map((mod) => {
          let Module = modules[mod];
          // console.log(Module);
          let module = Module();
          console.log(basename + mod);
          return (
            <Route key={mod} path={basename + mod}>
              {/* <Module basename={mod} /> */}
              {Module()}
            </Route>
          );
        })}
      </Routes>
    </>
  );
};

export const Routing = ({ modules, basename, children }:any) => {
  let location = useLocation();

  // console.log(location);

  // console.log("routing " + status.current);
  return (
    <>
      {/* <Router basename={basename}> */}
      {/* <Route path="/" component={App} /> */}
      <Routes>
        <Route path="/" children={children} /> 
        <MakeRoutes modules={modules} />
        {/* <Route path="/:id" children={children} /> */}
        {/* <Route exact path="/usages"> */}
        {/* <UsagesDisplay argList={argList} usageList={usageList} /> */}
        {/* </Route> */}
        {/* <LoadSample sample={sample} /> */}
        {/* <Route exact path="/:sample" component={LoadSample} />
    <Route path="/:sample/:caseId" component={LoadSample} /> */}
      </Routes>
      {/* </Router> */}
    </>
  );
};
