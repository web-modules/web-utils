import {
    faCheckSquare,
    faEdit,
    faTrash
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Form, Formik, useField } from "formik";
import React, { useEffect, useRef, useState } from "react";
// import * as PouchDB from "pouchdb";

const FieldEdit = ({ db, id, ...props }: any) => {
    console.log(props);
    const [field, meta] = useField({ name: id, ...props });
    const [dbField, setDbField]: any = useState(null);

    const onEditFieldLbl = () => {
        console.log("edit item");
    };
    const onDeleteFieldLbl = () => {
        console.log("delete item");
    };
    const onEditFieldVal = () => {
        console.log("edit item");
    };
    const onDeleteFieldVal = () => {
        console.log("delete item");
    };
    useEffect(() => {
        const fetchData = async () => {
            // You can await here
            const dbField = await db.get(id);
            setDbField(dbField);
        };
        fetchData();
    }, [db, id]);

    return (
        <>
            {dbField && (
                <label htmlFor={props.id || props.name}>
                    {dbField.label}
                    {/* <button> */}
                    <FontAwesomeIcon
                        icon={faEdit}
                        size="xs"
                        style={{ margin: "4px 0px 0px 10px" }}
                        onClick={onEditFieldLbl}
                    />
                    {/* </button> */}
                    <FontAwesomeIcon
                        icon={faTrash}
                        size="xs"
                        style={{ margin: "4px 0px 0px 10px" }}
                        onClick={onDeleteFieldLbl}
                    />
                </label>
            )}
            {dbField && (
                <input
                    className="text-input"
                    placeholder={dbField.desc}
                    type="text"
                    {...field}
                    {...props}
                />
            )}
        </>
    );
};



// const MyCheckbox = ({ children, ...props }) => {
//   const [field, meta] = useField({ ...props, type: "checkbox" });
//   return (
//     <>
//       <label className="checkbox">
//         <input {...field} {...props} type="checkbox" />
//         {children}
//       </label>
//       {meta.touched && meta.error ? (
//         <div className="error">{meta.error}</div>
//       ) : null}
//     </>
//   );
// };

// Styled components ....
// const StyledSelect = styled.select`
//   color: var(--blue);
// `;

// const StyledErrorMessage = styled.div`
//   font-size: 12px;
//   color: var(--red-600);
//   width: 400px;
//   margin-top: 0.25rem;
//   &:before {
//     content: "❌ ";
//     font-size: 10px;
//   }
//   @media (prefers-color-scheme: dark) {
//     color: var(--red-300);
//   }
// `;

// const StyledLabel = styled.label`
//   margin-top: 1rem;
// `;

// const MySelect = ({ label, ...props }) => {
//   // useField() returns [formik.getFieldProps(), formik.getFieldMeta()]
//   // which we can spread on <input> and alse replace ErrorMessage entirely.
//   const [field, meta] = useField(props);
//   return (
//     <>
//       <label htmlFor={props.id || props.name}>{label}</label>
//       <select {...field} {...props} />
//       {meta.touched && meta.error ? <div>{meta.error}</div> : null}
//     </>
//   );
// };

// And now we can use these
export const ListItemsForm = ({ coll }: any) => {
    const [items, setItems] = useState([]);
    let dbRef: any = useRef<any>();
    useEffect(() => {
        // dbRef.current = new PouchDB.default(coll);
        const fetchData = async () => {
            // You can await here
            const dbColl = await dbRef.current.allDocs({ include_docs: true });
            const dbCollItems = dbColl.rows.map((row: any) => row.doc);
            console.log(dbCollItems);
            setItems(dbCollItems);
        };
        fetchData();
    }, [coll]);
    return (
        <>
            <Formik
                initialValues={
                    {
                        // firstName: "",
                        // lastName: "",
                        // email: "",
                        // acceptedTerms: false, // added for our checkbox
                        // jobType: "" // added for our select
                    }
                }
                // validationSchema={Yup.object({
                //   firstName: Yup.string()
                //     .max(15, "Must be 15 characters or less")
                //     .required("Required"),
                //   lastName: Yup.string()
                //     .max(20, "Must be 20 characters or less")
                //     .required("Required"),
                //   email: Yup.string()
                //     .email("Invalid email addresss`")
                //     .required("Required"),
                //   acceptedTerms: Yup.boolean()
                //     .required("Required")
                //     .oneOf([true], "You must accept the terms and conditions."),
                //   jobType: Yup.string()
                //     // specify the set of valid values for job type
                //     // @see http://bit.ly/yup-mixed-oneOf
                //     .oneOf(
                //       ["designer", "development", "product", "other"],
                //       "Invalid Job Type"
                //     )
                //     .required("Required")
                // })}
                onSubmit={async (values, { setSubmitting }) => {
                    await new Promise((r) => setTimeout(r, 500));
                    setSubmitting(false);
                }}
            >
                <Form>
                    {items.map((item, i) => (
                        <FieldEdit db={dbRef.current} id={item._id} />
                    ))}

                    {/* <MySelect label="Job Type" name="jobType">
    <option value="">Select a job type</option>
    <option value="designer">Designer</option>
    <option value="development">Developer</option>
    <option value="product">Product Manager</option>
    <option value="other">Other</option>
  </MySelect> */}
                    {/* <MyCheckbox name="acceptedTerms">
    I accept the terms and conditions
  </MyCheckbox> */}
                </Form>
            </Formik>
        </>
    );
};

/**
 * no label  => only text input
 * no placeholder to disable text input
 * @param param0
 */
//  const TextInput = ({ label, ...props }:any) => {
//     // useField() returns [formik.getFieldProps(), formik.getFieldMeta()]
//     // which we can spread on <input> and alse replace ErrorMessage entirely.
//     const [field, meta]:any = useField(props);

//     return (
//         <>
//             {label && <FieldEdit label={label} />}
//             {props.placeholder && (
//                 <input className="text-input" {...field} {...props} />
//             )}
//             {meta.touched && meta.error ? (
//                 <div className="error">{meta.error}</div>
//             ) : null}
//         </>
//     );
// };

// export const AddItemForm = () => {
//     return (
//         <>
//             <Formik
//                 initialValues={{
//                     firstName: "",
//                     lastName: "",
//                     email: "",
//                     acceptedTerms: false, // added for our checkbox
//                     jobType: "" // added for our select
//                 }}
//                 onSubmit={async (values, { setSubmitting }) => {
//                     await new Promise((r) => setTimeout(r, 500));
//                     setSubmitting(false);
//                 }}
//             >
//                 <Form>
//                     <TextInput
//                         label={null}
//                         name="categ"
//                         type="text"
//                         placeholder="name"
//                         style={{
//                             float: "left",
//                             width: "20%",
//                             marginTop: "30px",
//                             marginRight: "20px"
//                         }}
//                     />
//                     <TextInput
//                         label={null}
//                         name="categ"
//                         type="text"
//                         placeholder="description"
//                         style={{
//                             float: "left",
//                             width: "35%",
//                             marginTop: "30px",
//                             marginRight: "20px"
//                         }}
//                     />
//                 </Form>
//             </Formik>
//             <button type="submit" style={{ marginTop: "30px", marginRight: "20px" }}>
//                 +
//         </button>{" "}
//         </>
//     );
// };
