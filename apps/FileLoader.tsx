import { useState, useEffect } from "react";
import { fetchRemoteUrl } from "../WebUtils";
// import { FillArgs, UsagesDisplay } from "../common/AppBasics";

const LOCALE_FILE_URL = "samples/sampleFile.txt";
const REMOTE_FILE_URL =
  "https://gist.githubusercontent.com/etienne1911/c8ebbb56597fba3c610d4fea825758c2/raw/external_sample.csv";

const ARGS: any = {
  fileUrl: ""
};

const USAGES: any = {
  localFile: "?fileUrl=" + LOCALE_FILE_URL,
  remoteFile: "?fileUrl=" + REMOTE_FILE_URL
};

const FileLoader = ({ fileUrl }: any) => {
  const [data, setData] = useState("no url provided");

  useEffect(() => {
    (async () => {
      if (fileUrl) {
        const remoteFileData: any = await fetchRemoteUrl(fileUrl);
        setData(remoteFileData);
      }
    })();
  });

  return (
    <>
      Loaded file content:
      <br />
      {data}
    </>
  );
};

const ArgsChecking = () => {};

export default ({ }) => {
  const [checksPassed, setChecksPassed] = useState(false);

  return (
    <>
    {/* <FillArgs argList={ARGS}>
      {checksPassed ? (
        <FileLoader fileUrl={ARGS.fileUrl} />
      ) : (
        <UsagesDisplay argList={ARGS} usageList={USAGES} />
      )}
    </FillArgs> */}
    </>
  );
};
