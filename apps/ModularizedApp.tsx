import React, { useEffect, useRef, useState } from "react";
import {
  BrowserRouter as Router,
  Route,
  useLocation
} from "react-router-dom";
import { MakeRoutes, FillArgs, Routing } from "../app-basics"; 
import { UsagesDisplay, DispArgs, DispLinks } from "../app-helpers";

/* A model to create a modularized app
 * supporting routes for each app submodule
 *
 * Example:
 * root: the main app
 * /help: a submodule/component listing app usage help
 * /about: ...
 *
 */

//If your app is modularized
// add any supported submodules here
const ROUTES: any = {
  Help: "/usages" // set a default value
};

//If your app takes arguments as input,
//add any supported url arguments to this list
const ARGS: any = {
  firstArg: "defaultValue", // set a default value
  secondArg: null // no default value
};

//If you want to describe some usage examples of your app,
//specify it here
const USAGES: any = {
  NoArgs: "?none",
  FirstArgOnly: "?firstArg=valueSet!",
  SecondArgOnly: "?secondArg=valueSet!",
  BothArgs: "?firstArg=valueSet&secondArg=valueSet"
};

export const ModularizedSampleApp = () => {
  let location = useLocation();
  let basename = useRef(location.pathname + "/");
  let usages = { ...USAGES };
  Object.keys(usages).forEach(
    (key) => (usages[key] = location.pathname + usages[key])
  );
  let usagesRef = useRef(usages);
  let modules = useRef({
    usages: () => <UsagesDisplay argList={ARGS} usagesRef={usagesRef} />
  });

  let args = ARGS; //{ ...ARGS };

  let routes = { ...ROUTES };
  Object.keys(routes).forEach(
    (key) => (routes[key] = location.pathname + routes[key])
  );

  useEffect(() => {
    // basename.current = location.pathname;
  }, []);
  console.log("basename: " + basename.current);
  console.log(routes);
  return (
    <>
      <FillArgs argList={args}>
        <h1>Modularized App</h1>
        <DispArgs args={args} />
      </FillArgs>
      <h2>Submodules</h2>
      <DispLinks links={routes} />
      <MakeRoutes basename={basename.current} modules={modules.current} />
    </>
  );
};
