import Apps from "../routing"
import { ModularizedSampleApp } from "./ModularizedApp"
import { StandaloneSampleApp } from "./StandaloneApp"

/**
 * Module Apps here
 */
export const MiscUtilsModule = () => {
    return (
        // <Routing>
        <Apps>
            <StandaloneSampleApp />
            <ModularizedSampleApp />
        </Apps>
        // </Routing>
    )
}