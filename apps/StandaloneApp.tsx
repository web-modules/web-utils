import React, { useRef } from "react";
import {
  BrowserRouter as Router,
  Route,
  Routes,
  useLocation
} from "react-router-dom";
import { FillArgs } from "../app-basics";
import { DispArgs, UsagesDisplay } from "../app-helpers"; 

/* This is a model to create a single app
 * supporting routing, url arguments and usages display
 */

//If your app takes arguments as input,
//add any supported url arguments to this list
const ARGS: any = {
  firstArg: "defaultValue", // set a default value
  secondArg: null // no default value
};

//If you want to describe some usage examples of your app,
//specify it here
const USAGES: any = {
  NoArgs: "?none",
  FirstArgOnly: "?firstArg=valueSet!",
  SecondArgOnly: "?secondArg=valueSet!",
  BothArgs: "?firstArg=valueSet&secondArg=valueSet"
};

export const Routing = ({ children }:any) => {
  let location = useLocation();

  // console.log("routing " + status.current);
  return (
    <Router basename={location.pathname}>
      {/* <Route path="/" component={App} /> */}
      <Routes>
        <Route path="/" children={children} />
      </Routes>
    </Router>
  );
};

export const StandaloneSampleApp = () => {
  let args = ARGS; //{ ...ARGS };
  let usagesRef = useRef(USAGES);

  return (
    <Routing>
      <FillArgs argList={args}>
        <h1>Single app template</h1>
        <DispArgs args={args} />
        <UsagesDisplay argList={args} usagesRef={usagesRef} />
      </FillArgs>
    </Routing>
  );
};
